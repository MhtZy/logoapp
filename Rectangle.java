package graphics;

public class Rectangle {

    private int x;
    private int y;
    private double height;
    private double width;
    private static int count;
    public static final int ANGELS=4;


    public static void main(String[] args) {

    }
    public Rectangle(Rectangle r){
        this(r.x,r.y,r.height,r.width);
    }
    public Rectangle(){
        this(0,0,0,0);
    }

    public Rectangle(double height, double width){
        this(0,0,height,width);

    }
    public Rectangle(int x, int y, double height, double width) {
            this.width=width<0?-width:width;
            this.height=height<0?-height:height;
            this.x=x;
            this.y=y;
            count++;
    }
    public static int getCount(){
        return count;
    }

    public void setHeight(double height) { this.height = height; }

    public void setPosition(int x, int y){ this.x=x; this.y=y;}

    public void grow(int d){this.height+=d; this.width+=d;}

    public void setX(int x){ this.x=x;}

    public void setWidth(double width) { this.width = width; }

    public void setY(int y) { this.y = y; }

    public double getHeight() { return height; }

    public double getWidth() { return width; }

    public int getX() { return x; }

    public int getY() { return y; }

    public void showInfos(){
        System.out.println("X degeriniz: "+ this.x);
        System.out.println("Y degeriniz: "+ this.y);
        System.out.format("Height degeriniz: %.2f \n",this.height);
        System.out.format("Width degeriniz: %.2f \n", this.width);
        System.out.format("Area of the rectangle is: %.2f \n",getArea(this.height,this.width));
        System.out.format("The perimeter of the rectangle is: %.2f \n",getPerimeter(this.height,this.width));

    }
    public double getArea(double height, double width){
        return height*width;
    }
    public double getPerimeter(double height, double width){
        return 2*(height+width);
    }
}
