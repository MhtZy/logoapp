package graphics;

public class Logo {
    public static final double	PI =	3.141592653589793;
    public static final double	E  =    2.718281828459045;
    //Declaring Arrays for storing the objects according to types
    private Rectangle[] rArray = new Rectangle[100];
    private Circle[] cArray = new Circle[100];
    private String[] strArray = new String[100];
    //counters
    private static int countRectArray;
    private static int countCirArray;
    private static int countStrArray;
    private static int countLogo;
    //class variables
    private Rectangle r = new Rectangle();
    private Circle c = new Circle();
    private String txt = new String();


    ///**** Constructions
    public Logo(){
        this(null,null,null);
    }
    public Logo(Rectangle r){
        this(r,null,null);
    }
    public Logo(Circle c){
        this(null,c,null);
    }
    public Logo(String txt){
        this(null,null,txt);
    }
    public Logo(Rectangle r, Circle c, String txt){
        this.r=r;
        this.c=c;
        this.txt=txt;
        countLogo++;
    }

    //******* Getters and Setters
    public void setR(Rectangle r){this.r=r;}
    public void setC(Circle c){this.c=c;}
    public void setR(String txt){this.txt=txt;}
    public Rectangle getR(){return this.r;}
    public Circle getC(){return this.c;}
    public String getTxt(){return this.txt;}
    public Rectangle[] getrArray(){return rArray;}
    public Circle[] getcArray(){return cArray;}
    public String[] getStrArray(){return strArray;}
    public int getCountRectArray(){return countRectArray;}
    public int getCountCirArray(){  return countCirArray;}
    public int getCountStrArray(){ return countStrArray;}
    public int getCountLogo(){return countLogo;}






    public double getAreaLogo(Rectangle r, Circle c, String txt){
        double area=0;
        if(this.r==r && this.c==c && this.txt.equals(txt)){
            area = (r.getHeight()*r.getWidth())+(PI*(c.getRadius()*c.getRadius()));;
        }else {area=0;}
        return area;
    }
    public void addArrays(Rectangle r, Circle c, String txt){

            rArray[countRectArray]= r;
            cArray[countCirArray]= c;
            strArray[countStrArray]=txt;
            countRectArray++;
            countCirArray++;
            countStrArray++;
    }

}
