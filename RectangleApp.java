package graphics;
import static graphics.Rectangle.getCount;
import static graphics.Rectangle.ANGELS;
public class RectangleApp {
    public static void main(String[] args) {

        Rectangle rect = new Rectangle(5,10,20,10);
        Rectangle rect2 = new Rectangle();
        Rectangle rect3 = new Rectangle(20,10);


        rect.showInfos();
        rect2.showInfos();
        rect3.showInfos();

    }
}
