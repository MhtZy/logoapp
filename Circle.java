package graphics;

public class Circle {
    public static final double	PI =	3.141592653589793;
    public static final double	E  =    2.718281828459045;
    private int x;
    private int y;
    private int radius;
    private static int count;
    public static int ANGELS;

    public Circle(Circle c){
        this(c.x,c.y,c.radius);
    }
    public Circle(){
        this(0,0,0);
    }
    public Circle(int radius){
        this(0,0,radius);
    }
    public Circle(int x, int y, int radius){
        this.x=x;
        this.y=y;
        this.radius=radius;
        count++;

    }

    public void setX(int x) { this.x = x; }

    public void setY(int y) { this.y = y; }

    public void setRadius(int radius) { this.radius = radius; }

    public void setPosition(int x, int y){this.x=y; this.y=y;}

    public double getAreaCirlce(int radius){ return PI*(radius*radius); }

    public double getPerimeter(int radius){ return 2*PI*radius; }

    public void grow(int d){this.radius+=d;}

    public int getCount(){ return count; }

    public int getRadius() { return radius;}

    public int getY() { return y; }

    public int getX() { return x; }

    public void showInfos(){
        System.out.println("X degeriniz: "+ this.x);
        System.out.println("Y degeriniz: "+ this.y);
        System.out.format("Radius degeriniz: %.2f \n",this.radius);
        System.out.format("Area of the Circle is: %.2f \n",getAreaCirlce(this.radius));
        System.out.format("The perimeter of the Circle is: %.2f \n",getPerimeter(this.radius));

    }
}
